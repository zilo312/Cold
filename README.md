# Cold

#### 有点小，仔细看，右上角绿的那个
![带鱼屏](https://images.gitee.com/uploads/images/2018/1127/210046_b8e8bd4d_20764.jpeg "带鱼屏.jpg")

#### 项目介绍
硬件监控小插件
#### 第三方库
1.OpenHardwareMonitor

#### 使用说明

1. VS2017 Buid
2. 管理员权限运行
4. 托到合适的位置

#### 下载安装包
1.[Cold.zip](https://gitee.com/scphbin/Cold/attach_files/download?i=187195&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F05%2FB4%2FPaAvDFv9PvOAAR2uAAKNGYn4TsU949.zip%3Ftoken%3D3635e08413e32fad07ff9116584cdac9%26ts%3D1543323379%26attname%3DCold.zip)

#### 参与贡献

1. Fork 本项目
2. 新建分支
3. 提交代码