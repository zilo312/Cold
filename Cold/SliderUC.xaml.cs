﻿using System.Windows.Controls;

namespace Cold
{
    /// <summary>
    /// SliderUC.xaml 的交互逻辑
    /// </summary>
    public partial class SliderUC : UserControl
    {
        public SliderUC()
        {
            InitializeComponent();
        }
        public string NameStr
        {
            get
            {
                return this.NameLabel.Content.ToString();
            }
            set
            {

                this.NameLabel.Content = value;
            }
        }

        public double MaxValue
        {
            get
            {
                return this.valuePB.Maximum;
            }
            set
            {

                this.valuePB.Maximum = value;
            }
        }

        public double MinValue
        {
            get
            {
                return this.valuePB.Minimum;
            }
            set
            {

                this.valuePB.Minimum = value;
            }
        }

        public double Value
        {
            get
            {
                return this.valuePB.Value;
            }
            set
            {

                this.valuePB.Value = value;
            }
        }

        public string ValueStr
        {
            get
            {
                return this.ValueLabel.Content.ToString();
            }
            set
            {

                this.ValueLabel.Content = value;
            }

        }
    }
}
