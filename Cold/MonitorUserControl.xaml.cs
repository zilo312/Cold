﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cold
{
    /// <summary>
    /// MonitorUserControl.xaml 的交互逻辑
    /// </summary>
    public partial class MonitorUserControl : UserControl
    {
        public MonitorUserControl()
        {
            InitializeComponent();
        }

        public string NameStr
        {
            get
            {
                return this.nameLabel.Content.ToString();
            }
            set
            {

                this.nameLabel.Content = value;
            }
        }


        public string nameStr_1
        {
            get
            {
                return this.SUC1.NameStr;
            }
            set
            {

                this.SUC1.NameStr = value;
            }
        }

        public double valueMax_1
        {
            get
            {
                return this.SUC1.MaxValue;
            }
            set
            {

                this.SUC1.MaxValue = value;
            }
        }



        public double valueMin_1
        {
            get
            {
                return this.SUC1.MinValue;
            }
            set
            {

                this.SUC1.MinValue = value;
            }
        }


        public double value_1
        {
            get
            {
                return this.SUC1.Value;
            }
            set
            {

                this.SUC1.Value = value;
            }
        }


        public string valueStr_1
        {
            get
            {
                return this.SUC1.ValueStr;
            }
            set
            {

                this.SUC1.ValueStr = value;
            }
        }



        public string nameStr_2
        {
            get
            {
                return this.SUC2.NameStr;
            }
            set
            {

                this.SUC2.NameStr = value;
            }
        }

        public double valueMax_2
        {
            get
            {
                return this.SUC2.MaxValue;
            }
            set
            {

                this.SUC2.MaxValue = value;
            }
        }



        public double valueMin_2
        {
            get
            {
                return this.SUC2.MinValue;
            }
            set
            {

                this.SUC2.MinValue = value;
            }
        }


        public double value_2
        {
            get
            {
                return this.SUC2.Value;
            }
            set
            {

                this.SUC2.Value = value;
            }
        }



        public string valueStr_2
        {
            get
            {
                return this.SUC2.ValueStr;
            }
            set
            {

                this.SUC2.ValueStr = value;
            }
        }

    }
}
